## Git Branches Usage

	Find a reason why you should use git branches
		Answer: to do work without messing with main line
	
	Describe how you use git branches on that repository
		Answer: -git branch <BRANCHNAME> -> to create a new branch	... (1)
			-git checkout <BRANCHNAME> -> to switch to that branch	... (2)
			-git checkout -b <BRANCHNAME> -> (1) and (2) simplified
			-git branch -v -> check which branch I am working on
			-start working on current branch
			-git add . && git commit
			-git push -u origin <BRANCHNAME> -> to push the commit to current branch
			-git merge <BRANCHNAME> -> merge current branch to master branch

## Git Revert Usage

	Find a scenario so you should use git revert
		Answer: when I accidently push "unwanted" commit to branch
			or when tracking down a bug.
	
	Describe how you use git revert on that repository
		Answer:	-git log -> to know list of commits and its hash in reverse chronological order
			-git checkout <commit hash> -> to look around the state of the file on that commit snapshot
			-git revert <commit hash> -> to rollback to the commit snapshot that I want
			-resolve the conflict
			-git add . -> add to the staging area
			-git revert --continue -> to save changes of revert process
			-git log -> to see whether the revert process was successful or not