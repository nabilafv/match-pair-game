import javax.swing.JButton;
import javax.swing.ImageIcon;

/**
 * @author Nabila Febri Viola
 * A class for creating Card in form of JButton.
 */
public class Card extends JButton {

    /**
     * Front card icon.
     */
    private ImageIcon face;

    /**
     * Back card icon.
     */
    private ImageIcon back;

    /**
     * Constructor. Initializes a face down card.
     */
    public Card(ImageIcon face, ImageIcon back) {
        setIcon(back); //initialize all cards face down
        this.face = face;
        this.back = back;
    }

    /**
     * This method is used to get the front card icon.
     * @return ImageIcon This returns the front card icon.
     */
    public ImageIcon getFace() {
        return face;
    }

    /**
     * This method is used to flip the card so that it shows the front card icon.
     */
    public void faceUp() {
        setIcon(face);
    }

    /**
     * This method is used to flip the card so that it shows the back card icon.
     */
    public void faceDown() {
        setIcon(back);
    }
}