

    /**
     * This method is used to get the game panel that consist of cards.
     * @return JPanel This returns the card panel.
     */
    public JPanel getPanel() {
        return gamePnl;
    }

    /**
     * This method is used to get the number of tries label.
     * @return JLabel This returns the number of tries label.
     */
    public JLabel getTriesLbl() {
        return triesLbl;
    }

    /**
     * This method is used to load the image icon of the cards.
     * @return ImageIcon[] This returns an array of image icons for the cards.
     */
    private ImageIcon[] loadCardIcons() {
        ImageIcon[] cardIcons = new ImageIcon[18];
        try {
            for (int i = 1; i < 19; i++) {
                String fileName = ".\\img\\img" + i + ".png";
                BufferedImage image = ImageIO.read(new File(fileName));
                cardIcons[i-1] = new ImageIcon(fileName);
            }
        } catch (IOException e) {
            System.out.println("There is a card that doesn't have front image icon");
            System.exit(0);
        } finally {
            return cardIcons;
        }
    }

    /**
     * This method is used to create the game panel that consists of cards.
     * @return JPanel This returns the game panel.
     */
    private JPanel cardPanel() {

        ImageIcon backIcon = null;
        String fileNameBg = "";

        try {
            String fileName = ".\\img\\background.png";
            BufferedImage image = ImageIO.read(new File(fileName));
            fileNameBg = fileName;
        } catch (IOException e) {
            System.out.println("Hidden background not found");
            System.exit(0);
        }

        try {
            String fileName = ".\\img\\back.png";
            BufferedImage image = ImageIO.read(new File(fileName));
            backIcon = new ImageIcon(fileName);
        } catch (IOException e) {
            System.out.println("Cards don't have back image icon");
            System.exit(0);
        }

        ImageIcon background = new ImageIcon(fileNameBg);
        JPanel gamePnl = new JPanel(new GridLayout(6, 6)) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(background.getImage(), 0, 0, null);
            }
        };
        gamePnl.setPreferredSize(new Dimension(1400, 850));

        cards = new ArrayList<Card>();

        int x = 0;
        do {
            for (int i = 0; i < 18; i++) {
                cards.add(new Card(cardIcons[i], backIcon));
            }
            x++;
        } while (x <= 1);

        Collections.shuffle(cards);

        for (int i = 0; i < 36; i++) {
            Card card = cards.get(i);
            card.addActionListener(e -> {
                if(card1 == null) {
                    card1 = card;
                    card1.faceUp();
                } else if(card2 == null && card != card1) {
                    card2 = card;
                    card2.faceUp();
                    numOfTries++;
                    triesLbl.setText("Number of Tries: " + numOfTries);
                    timer.start();
                }
            });
            gamePnl.add(card);
        }
        return gamePnl;
    }

    /**
     * This method is used to check whether the game is completed or not
     * @return true if the game is completed, false otherwise.
     */
    private boolean isGameWon() {
        for(Card card : cards) {
            if(card.isVisible()) {
                return false;
            }
        }
        return true;
    }
}