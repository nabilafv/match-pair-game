import javax.swing.SwingUtilities;

/**
 * @author Nabila Febri Viola
 * A class for running the GUI.
 */
public class Main {
	
	/**
	 * Main method. Run the GUI in other thread.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			MatchPairGame game = new MatchPairGame();
		});
	}
}