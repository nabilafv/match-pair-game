import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

/**
 * @author Nabila Febri Viola
 * A class for creating GUI of match pair game.
 */
public class MatchPairGame {

    /**
     * Constructor. Creates the game frame with its components.
     */
    public MatchPairGame() {
        JFrame frame = new JFrame("Remember the Hero");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(1500, 970));
        frame.getContentPane().setBackground(Color.DARK_GRAY);
        frame.setResizable(false);
        frame.setLayout(new BorderLayout());

        GamePanel gamePnl = new GamePanel();

        JPanel buttonPnl = new JPanel();
        buttonPnl.setBackground(Color.LIGHT_GRAY);

        JButton restartBtn = new JButton("Play Again?");
        restartBtn.setPreferredSize(new Dimension(170, 40));
        restartBtn.setFont(restartBtn.getFont().deriveFont(23.0f));
        restartBtn.setForeground(Color.MAGENTA);
        restartBtn.setBackground(Color.WHITE);
        restartBtn.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                Container container = frame.getContentPane();
                container.removeAll();
                GamePanel gamePnl2 = new GamePanel();
                container.add(gamePnl2.getPanel(), BorderLayout.NORTH);
                container.add(buttonPnl, BorderLayout.CENTER);
                container.add(gamePnl2.getTriesLbl(), BorderLayout.SOUTH);
                frame.validate(); // Changed here
                frame.repaint(); // Changed here
            });
        });

        JButton exitBtn = new JButton("Exit");
        exitBtn.setPreferredSize(new Dimension(170, 40));
        exitBtn.setFont(exitBtn.getFont().deriveFont(23.0f));
        exitBtn.setForeground(Color.RED);
        exitBtn.setBackground(Color.WHITE);
        exitBtn.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                System.exit(0);
            });
        });

        buttonPnl.add(restartBtn);
        buttonPnl.add(exitBtn);

        frame.add(gamePnl.getPanel(), BorderLayout.NORTH);
        frame.add(buttonPnl, BorderLayout.CENTER);
        frame.add(gamePnl.getTriesLbl(), BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
    }
}